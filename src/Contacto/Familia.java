/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Contacto;

import java.util.Scanner;

/**
 *
 * @author ihcontrol
 */
public class Familia extends Persona{
    Scanner leer = new Scanner (System.in); 
    //Declaración de atributos 
    private int codigo; 
    private String Nombre;
    private String Apellido;
    private int Edad;
    private int Telefono;
    private int Celular;
    private String Email;
    private String Direccion;
    private String Genero;

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Nombre the Nombre to set
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    /**
     * @return the Apellido
     */
    public String getApellido() {
        return Apellido;
    }

    /**
     * @param Apellido the Apellido to set
     */
    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    /**
     * @return the Edad
     */
    public int getEdad() {
        return Edad;
    }

    /**
     * @param Edad the Edad to set
     */
    public void setEdad(int Edad) {
        this.Edad = Edad;
    }

    /**
     * @return the Telefono
     */
    public int getTelefono() {
        return Telefono;
    }

    /**
     * @param Telefono the Telefono to set
     */
    public void setTelefono(int Telefono) {
        this.Telefono = Telefono;
    }

    /**
     * @return the Celular
     */
    public int getCelular() {
        return Celular;
    }

    /**
     * @param Celular the Celular to set
     */
    public void setCelular(int Celular) {
        this.Celular = Celular;
    }

    /**
     * @return the Email
     */
    public String getEmail() {
        return Email;
    }

    /**
     * @param Email the Email to set
     */
    public void setEmail(String Email) {
        this.Email = Email;
    }

    /**
     * @return the Direccion
     */
    public String getDireccion() {
        return Direccion;
    }

    /**
     * @param Direccion the Direccion to set
     */
    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    /**
     * @return the Genero
     */
    public String getGenero() {
        return Genero;
    }

    /**
     * @param Genero the Genero to set
     */
    public void setGenero(String Genero) {
        this.Genero = Genero;
    }
    
    Principal.TestAgenda Agenda= new Principal.TestAgenda();
    @Override
    public void Agregar() {
        System.out.println("Numero "+(Agenda.getFamiliaC()+1));
        setCodigo(Agenda.getFamiliaC()+1);
         System.out.println("Ingrese el nombre de su amigo");
         setNombre(leer.next());
         System.out.println("Ingrese el apellido de su amigo");
         setApellido(leer.next());
         System.out.println("Ingrese la edad de su amigo");
         setEdad(leer.nextInt());
         System.out.println("Ingrese el númnero telefonico de su amigo");
         setTelefono(leer.nextInt());
         System.out.println("Ingrese el número celular de su amigo");
         setCelular(leer.nextInt());
         System.out.println("Ingrse el email de su amigo");
         setEmail(leer.next());
         System.out.println("Ingrese la dirección de residencia de su amigo");
         setDireccion(leer.next());
         System.out.println("Ingrese el genero de su amigo");
         setGenero(leer.next());
         //Conocer la cantidad de caracteres de tipo texto
         System.out.println("La cantidad de caracteres de tipo texto es:"+Nombre.length()+Apellido.length()+Email.length()+Direccion.length()+Genero.length()); // conocer la canitidad de caracteres
         //Validación de letra capital
         //Nombre
         String mayusculan=Nombre.charAt(0)+"";//La cadena que queremos transformar
         //Obtenemos el primer caracteres y concatenamos "" para que se transforme el char en String 
         mayusculan=mayusculan.toUpperCase();
         Nombre=Nombre.replaceFirst(Nombre.charAt(0)+"", mayusculan);
         //Reemplazamos el primer caracter con mayuscula
         //Apellido
         String mayusculaa = Apellido.charAt(0)+"";//La cadena que queremos trasformar 
         //Obtenemos el primer caracteres y concatenamos "" para que se transforme el char en String 
         mayusculaa=mayusculaa.toUpperCase();
         Apellido=Apellido.replaceFirst(Apellido.charAt(0)+"", mayusculaa);
         //Reemplazamos el primer caracter con mayuscula//Obtenemos el primer caracteres y concatenamos "" para que se transforme el char en String 
         mayusculaa=mayusculaa.toUpperCase();
         Apellido=Apellido.replaceFirst(Apellido.charAt(0)+"", mayusculaa);
         //Reemplazamos el primer caracter con mayuscula
          //Genero
         String mayusculag = Genero.charAt(0)+"";//La cadena que queremos trasformar 
         //Obtenemos el primer caracteres y concatenamos "" para que se transforme el char en String 
         mayusculag=mayusculag.toUpperCase();
         Genero=Genero.replaceFirst(Genero.charAt(0)+"", mayusculag);
         //Reemplazamos el primer caracter con mayuscula//Obtenemos el primer caracteres y concatenamos "" para que se transforme el char en String 
         mayusculag=mayusculag.toUpperCase();
         Genero=Genero.replaceFirst(Genero.charAt(0)+"", mayusculag);
         //Reemplazamos el primer caracter con mayuscula
    }

    @Override
    public void Modificar() {
        System.out.println("Ingrese el numero que quiere modificar");
        setCodigo(leer.nextInt());
        System.out.println("Ingrese el nombre");
        setNombre(leer.next());
        System.out.println("Ingrese el apellido");
        setApellido(leer.next());
        System.out.println("Ingrese la edad");
        setEdad(leer.nextInt());
        System.out.println("Ingrese el número telefonico");
        setTelefono(leer.nextInt());
        System.out.println("Ingrese el número celular");
        setCelular(leer.nextInt());
        System.out.println("Ingrse el numero de trabajo");
        setEmail(leer.next());
        System.out.println("Ingrese la dirección");
        setDireccion(leer.next());
        System.out.println("Ingrese el genero");
        setGenero(leer.next());
        //Validación de letra capital
         //Nombre
         String mayusculan=Nombre.charAt(0)+"";//La cadena que queremos transformar
         //Obtenemos el primer caracteres y concatenamos "" para que se transforme el char en String 
         mayusculan=mayusculan.toUpperCase();
         Nombre=Nombre.replaceFirst(Nombre.charAt(0)+"", mayusculan);
         //Reemplazamos el primer caracter con mayuscula
         //Apellido
         String mayusculaa = Apellido.charAt(0)+"";//La cadena que queremos trasformar 
         //Obtenemos el primer caracteres y concatenamos "" para que se transforme el char en String 
         mayusculaa=mayusculaa.toUpperCase();
         Apellido=Apellido.replaceFirst(Apellido.charAt(0)+"", mayusculaa);
         //Reemplazamos el primer caracter con mayuscula//Obtenemos el primer caracteres y concatenamos "" para que se transforme el char en String 
         mayusculaa=mayusculaa.toUpperCase();
         Apellido=Apellido.replaceFirst(Apellido.charAt(0)+"", mayusculaa);
         //Reemplazamos el primer caracter con mayuscula
          //Genero
         String mayusculag = Genero.charAt(0)+"";//La cadena que queremos trasformar 
         //Obtenemos el primer caracteres y concatenamos "" para que se transforme el char en String 
         mayusculag=mayusculag.toUpperCase();
         Genero=Genero.replaceFirst(Genero.charAt(0)+"", mayusculag);
         //Reemplazamos el primer caracter con mayuscula//Obtenemos el primer caracteres y concatenamos "" para que se transforme el char en String 
         mayusculag=mayusculag.toUpperCase();
         Genero=Genero.replaceFirst(Genero.charAt(0)+"", mayusculag);
         //Reemplazamos el primer caracter con mayuscula
    }

    @Override
    public void Consultar() {
        System.out.println("Agenda de Familia ");
    }

    @Override
    public void Especifica() {
        System.out.println("Ingrese el nombre del contacto que busca");
        setNombre(leer.next());
        System.out.println("Ingrese el apellido del contacto que busca ");
        setApellido(leer.next());
    
    }
    
}
