/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import java.util.Scanner; 

/**
 *
 * @author ihcontrol
 */
public class TestAgenda {
    private static int AmigoC=0;
    private static int CompTrabajoC=0;
    private static int ConocidoC=0;
    private static int FamiliaC=0;

    public static void main(String []args){
        int opc;
        int ContadorA=0;
        int ContadorComp=0;
        int ContadorC=0;
        int ContadorF=0;
        String opc1, nombre, apellido;
        String DatosAmigo[][]=new String [100][100];
        String DatosCompTrabajo[][]=new String [100][100];
        String DatosConocido[][]= new String [100][100];
        String DatosFamilia[][]= new String [100][100];
        Scanner Leer = new Scanner (System.in);
        
        //Ciclo
        do 
        {
            //Mensajes 
            System.out.println("Bienvenido, digite el numero que corresponde a la persona sobre la cual desea realizar la opcion");
            System.out.println("0) Salir");//Caso0
            System.out.println("1) Amigos"); //Caso1
            System.out.println("2) Compañeros De Trabajo"); //Caso2
            System.out.println("3) Conocidos"); //Caso3
            System.out.println("4) Familia"); //Caso4
            
            opc=Leer.nextInt();
            switch (opc) //Utilizamos el switch
                
            {
                case 1: 
                    int opcA;
                    String opcA1;
                    //Ciclo Amigos
                    do 
                    {
                        Contacto.Amigo a=new Contacto.Amigo();
                        //Mensajes
                        System.out.println("0) Salir");
                        System.out.println("1) Agregar"); //Caso1
                        System.out.println("2) Modificar"); //Caso2
                        System.out.println("3) Consultar"); //Caso3
                        System.out.println("4) Consultar Especifica " ); //Caso4
                        opcA=Leer.nextInt();
                        switch (opcA)
                        {
                        
                            case 1:
                                //Creamos el objeto
                                setAmigoC(getAmigoC()+1);
                                ContadorA++;
                                
                                a.Agregar();
                                DatosAmigo[ContadorA-1][0]=String.valueOf(a.getCodigo());
                                DatosAmigo[ContadorA-1][1]=a.getNombre();
                                DatosAmigo[ContadorA-1][2]=a.getApellido();
                                DatosAmigo[ContadorA-1][3]=String.valueOf(a.getEdad());
                                DatosAmigo[ContadorA-1][4]=String.valueOf(a.getTelefono());
                                DatosAmigo[ContadorA-1][5]=String.valueOf(a.getCelular());
                                DatosAmigo[ContadorA-1][6]=String.valueOf(a.getEmail());
                                DatosAmigo[ContadorA-1][7]=a.getDireccion();
                                DatosAmigo[ContadorA-1][8]=a.getGenero();
                                        
                                break;
                            case 2:
                                //Creamos el objeto
                               
                                a.Modificar();
                                DatosAmigo[a.getCodigo()][0]=String.valueOf(a.getCodigo());
                                DatosAmigo[a.getCodigo()][1]=a.getNombre();
                                DatosAmigo[a.getCodigo()][2]=a.getApellido();
                                DatosAmigo[a.getCodigo()][3]=String.valueOf(a.getEdad());
                                DatosAmigo[a.getCodigo()][4]=String.valueOf(a.getTelefono());
                                DatosAmigo[a.getCodigo()][5]=String.valueOf(a.getCelular());
                                DatosAmigo[a.getCodigo()][6]=a.getEmail();
                                DatosAmigo[a.getCodigo()][7]=a.getDireccion();
                                DatosAmigo[a.getCodigo()][8]=a.getGenero();
                                break;
                            case 3:
                                
                                a.Consultar();
                                for (int i=0; i<getAmigoC(); i++)
                                {
                                    for (int x=0; x<9;x++)
                                    {
                                        System.out.print(DatosAmigo[i][x]+" ");
                                    }
                                    System.out.println("");
                                }
                                break;
                            case 4:
                                a.Especifica();
                                nombre=a.getNombre();
                                apellido= a.getApellido();
                                for (int i=0; i<getAmigoC();i++)
                                {
                                    for (int x=0; x<9; x++)
                                    {
                                        if (DatosAmigo[i][1].compareTo(nombre)==0 && DatosAmigo[i][2].compareTo(apellido)==0)
                                        {
                                            System.out.print(DatosAmigo[i][x]+" ");
                                        }
                                    }
                                    System.out.println("");
                                }
                                break;
                            default:
                             //Si la opcion no esta disponible 
                                System.out.println("La opcion no esta disponible");
                                break;
                        }
                        
                        } while (opcA!=0);
                    break;
                case 2:
                    int opcC;
                    String opcC1;
                    //Ciclo Compañeros de trabajo
                    do 
                    {
                        Contacto.Compañero_Trabajo c=new Contacto.Compañero_Trabajo();
                        //Mensajes
                        System.out.println("0) Salir");
                        System.out.println("1) Agregar"); //Caso1
                        System.out.println("2) Modificar"); //Caso2
                        System.out.println("3) Consultar"); //Caso3
                        System.out.println("4) Consultar Especifica " ); //Caso3
                        opcC=Leer.nextInt();
                        switch (opcC)
                        {
                            
                            case 1:
                                //Creamos el objeto
                                setCompTrabajoC(getCompTrabajoC()+1);
                                ContadorComp++;
                                
                                c.Agregar();
                                DatosCompTrabajo[ContadorComp-1][0]=String.valueOf(c.getCodigo());
                                DatosCompTrabajo[ContadorComp-1][1]=c.getNombre();
                                DatosCompTrabajo[ContadorComp-1][2]=c.getApellido();
                                DatosCompTrabajo[ContadorComp-1][3]=String.valueOf(c.getEdad());
                                DatosCompTrabajo[ContadorComp-1][4]=String.valueOf(c.getTelefono());
                                DatosCompTrabajo[ContadorComp-1][5]=String.valueOf(c.getCelular());
                                DatosCompTrabajo[ContadorComp-1][6]=c.getEmail();
                                DatosCompTrabajo[ContadorComp-1][7]=c.getDireccion();
                                
                                break;
                            case 2:
                                //Creamos el objeto
                                c.Modificar();
                                DatosCompTrabajo[c.getCodigo()][0]=String.valueOf(c.getCodigo());
                                DatosCompTrabajo[c.getCodigo()][1]=c.getNombre();
                                DatosCompTrabajo[c.getCodigo()][2]=c.getApellido();
                                DatosCompTrabajo[c.getCodigo()][3]=String.valueOf(c.getEdad());
                                DatosCompTrabajo[c.getCodigo()][4]=String.valueOf(c.getTelefono());
                                DatosCompTrabajo[c.getCodigo()][5]=String.valueOf(c.getCelular());
                                DatosCompTrabajo[c.getCodigo()][6]=c.getEmail();
                                DatosCompTrabajo[c.getCodigo()][7]=c.getDireccion();
                                DatosCompTrabajo[c.getCodigo()][0]=c.getGenero();
                                break;
                            case 3:
                                //Creamos el objeto
                                c.Consultar();
                                for (int i=0; i<getCompTrabajoC(); i++)
                                {
                                    for (int y=0; y<9;y++)
                                    {
                                        System.out.println(DatosCompTrabajo[i][y]+" ");
                                    }
                                    System.out.println("");
                                }
                                break;
                            case 4:
                                c.Especifica();
                                nombre=c.getNombre();
                                apellido=c.getApellido();
                                for (int i=0; i<getCompTrabajoC();i++)
                                {
                                    for (int z=0; z<9; z++)
                                    {
                                        if (DatosCompTrabajo [i][1].compareTo(nombre)==0 && DatosCompTrabajo[i][2].compareTo(apellido)==0)
                                        {
                                            System.out.print(DatosCompTrabajo[i][z]+" ");
                                        }
                                    }
                                    System.out.println("");
                                }
                                break;
                            default:
                             //Si la opcion no esta disponible 
                                System.out.println("La opcion no esta disponible");
                                break;
                        }
                        
                    }while (opcC!=0);
                    break;
                    
                case 3: 
                    int opcCo;
                    String opcCo1;
                    //Ciclo Conocidos
                    do 
                    {
                        Contacto.Conocido co= new Contacto.Conocido();
                        //Opciones
                        System.out.println("0) Salir");
                        System.out.println("1) Agregar"); //Caso1
                        System.out.println("2) Modificar"); //Caso2
                        System.out.println("3) Consultar"); //Caso3
                        System.out.println("4) Consultar Especifica " ); //Caso3
                        opcCo=Leer.nextInt();
                        switch (opcCo)
                        {
                            case 1:
                                //Creamos el objeto
                                setConocidoC(getConocidoC()+1);
                                ContadorC++;
                                
                                co.Agregar();
                                DatosConocido[ContadorC-1][0]=String.valueOf(co.getCodigo());
                                DatosConocido[ContadorC-1][1]=co.getNombre();
                                DatosConocido[ContadorC-1][2]=co.getApellido();
                                DatosConocido[ContadorC-1][3]=String.valueOf(co.getEdad());
                                DatosConocido[ContadorC-1][4]=String.valueOf(co.getTelefono());
                                DatosConocido[ContadorC-1][5]=String.valueOf(co.getCelular());
                                DatosConocido[ContadorC-1][6]=co.getEmail();
                                DatosConocido[ContadorC-1][0]=co.getDireccion();
                                DatosConocido[ContadorC-1][0]=co.getGenero();
                                
                                break;
                               
                            case 2:
                                //Creamos el objeto
                                
                                co.Modificar();
                                DatosConocido[co.getCodigo()][0]=String.valueOf(co.getCodigo());
                                DatosConocido[co.getCodigo()][1]=co.getNombre();
                                DatosConocido[co.getCodigo()][2]=co.getApellido();
                                DatosConocido[co.getCodigo()][3]=String.valueOf(co.getEdad());
                                DatosConocido[co.getCodigo()][4]=String.valueOf(co.getTelefono());
                                DatosConocido[co.getCodigo()][5]=String.valueOf(co.getCelular());
                                DatosConocido[co.getCodigo()][0]=co.getEmail();
                                DatosConocido[co.getCodigo()][0]=co.getDireccion();
                                DatosConocido[co.getCodigo()][0]=co.getGenero();
                                break;
                            case 3: 
                                co.Consultar();
                                for (int i=0; i<getConocidoC(); i++)
                                {
                                    for (int m=0; m<9;m++)
                                    {
                                        System.out.print(DatosConocido[i][m]+" ");
                                    }
                                    System.out.println("");
                                }
                                break;
                            case 4: 
                                co.Especifica();
                                nombre=co.getNombre();
                                apellido=co.getApellido();
                                for (int i=0; i<getConocidoC(); i++)
                                {
                                    for (int m=0; m<9; m++)
                                    {
                                        if (DatosConocido[i][1].compareTo(nombre)==0 && DatosConocido[i][2].compareTo(apellido)==0)
                                        {
                                            System.out.print(DatosConocido[i][m]+" ");
                                        }
                                        System.out.println("");
                                    }
                                }
                                break;
                            default:
                            //Si la opcion no esta disponible 
                            System.out.println("La opcion no esta disponible");
                            break;
                        }
                        } while (opcCo!=0); 
                    break;
                case 4: 
                    int opcF;
                    int opcF1;
                    //Ciclo Familia
                    do 
                    {
                        Contacto.Familia f=new Contacto.Familia();
                        //Opciones 
                        System.out.println("0) Salir");
                        System.out.println("1) Agregar"); //Caso1
                        System.out.println("2) Modificar"); //Caso2
                        System.out.println("3) Consultar"); //Caso3
                        System.out.println("4) Consultar Especifica " ); //Caso3
                        opcF=Leer.nextInt();
                        switch (opcF)
                        {
                        
                            
                            case 1:
                                //Creamos el objeto
                                setFamiliaC(getFamiliaC()+1);
                                ContadorF++;
                                
                                f.Agregar();
                                DatosFamilia[ContadorF-1][0]=String.valueOf(f.getCodigo());
                                DatosFamilia[ContadorF-1][1]=f.getNombre();
                                DatosFamilia[ContadorF-1][2]=f.getApellido();
                                DatosFamilia[ContadorF-1][3]=String.valueOf(f.getEdad());
                                DatosFamilia[ContadorF-1][4]=String.valueOf(f.getTelefono());
                                DatosFamilia[ContadorF-1][5]=String.valueOf(f.getCelular());
                                DatosFamilia[ContadorF-1][6]=String.valueOf(f.getEmail());
                                DatosFamilia[ContadorF-1][7]=f.getDireccion();
                                DatosFamilia[ContadorF-1][8]=f.getGenero();
                                
                                break;
                            case 2: 
                                //Creamos el objeto
                                
                                f.Modificar();
                                DatosFamilia[f.getCodigo()][0]=String.valueOf(f.getCodigo());
                                DatosFamilia[f.getCodigo()][1]=f.getNombre();
                                DatosFamilia[f.getCodigo()][2]=f.getApellido();
                                DatosFamilia[f.getCodigo()][3]=String.valueOf(f.getEdad());
                                DatosFamilia[f.getCodigo()][4]=String.valueOf(f.getTelefono());
                                DatosFamilia[f.getCodigo()][5]=String.valueOf(f.getCelular());
                                DatosFamilia[f.getCodigo()][6]=String.valueOf(f.getEmail());
                                DatosFamilia[f.getCodigo()][7]=f.getDireccion();
                                DatosFamilia[f.getCodigo()][8]=f.getGenero();
                                break;
                            case 3: 
                                
                                f.Consultar();
                                for (int i=0; i<getFamiliaC(); i++)
                                {
                                    for (int x=0; x<9;x++)
                                    {
                                        System.out.print(DatosFamilia[i][x]+" ");
                                    }
                                    System.out.println("");
                                }
                                break;
                            case 4:
                                f.Especifica();
                                nombre=f.getNombre();
                                apellido= f.getApellido();
                                for (int i=0; i<getFamiliaC();i++)
                                {
                                    for (int x=0; x<9; x++)
                                    {
                                        if (DatosFamilia[i][1].compareTo(nombre)==0 && DatosFamilia[i][2].compareTo(apellido)==0)
                                        {
                                            System.out.print(DatosFamilia[i][x]+" ");
                                        }
                                         System.out.println("");
                                    }
                                   
                                }
                                break;
                            default:
                             //Si la opcion no esta disponible 
                                System.out.println("La opcion no esta disponible");
                                break;
                        }
                        
                        } while (opcF!=0);
                    break;
            }
        
   }
        while (opc!=0);
}

    /**
     * @return the AmigoC
     */
    public static int getAmigoC() {
        return AmigoC;
    }

    /**
     * @param aAmigoC the AmigoC to set
     */
    public static void setAmigoC(int aAmigoC) {
        AmigoC = aAmigoC;
    }

    /**
     * @return the CompTrabajoC
     */
    public static int getCompTrabajoC() {
        return CompTrabajoC;
    }

    /**
     * @param aCompTrabajoC the CompTrabajoC to set
     */
    public static void setCompTrabajoC(int aCompTrabajoC) {
        CompTrabajoC = aCompTrabajoC;
    }

    /**
     * @return the ConocidoC
     */
    public static int getConocidoC() {
        return ConocidoC;
    }

    /**
     * @param aConocidoC the ConocidoC to set
     */
    public static void setConocidoC(int aConocidoC) {
        ConocidoC = aConocidoC;
    }

    /**
     * @return the FamiliaC
     */
    public static int getFamiliaC() {
        return FamiliaC;
    }

    /**
     * @param aFamiliaC the FamiliaC to set
     */
    public static void setFamiliaC(int aFamiliaC) {
        FamiliaC = aFamiliaC;
    }
}

    



    
                    
  
                        
      
